<div class="">
    

            <div id="slider" class="sl-slider-wrapper">

        <div class="sl-slider">
        
          <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
            <div class="sl-slide-inner">
              <div class="bg-img bg-img-1"></div>
              <h2><a href="#">Kos Chiqitta Nugroho Corporation</a></h2>
              <blockquote>              
              <p>Kos Chiqitta Nugroho Corporation adalah sebuah pengelola kos yang terletak di kota Malang. Kos Chiqitta Nugroho Corporation ini juga sudah memiliki beberapa cabang di daerah Malang.</p>
              
              </blockquote>
            </div>
          </div>
          
          <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="10" data-slice2-rotation="-15" data-slice1-scale="1.5" data-slice2-scale="1.5">
            <div class="sl-slide-inner">
              <div class="bg-img bg-img-2"></div>
              <h2><a href="#">Ruang Tamu</a></h2>
              <blockquote>              
              <p>Memiliki ruang tamu yang bisa anda gunakan untuk berkumpul anda dalam pertemuan keluarga.</p>

              </blockquote>
            </div>
          </div>
          
          <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="3" data-slice2-rotation="3" data-slice1-scale="2" data-slice2-scale="1">
            <div class="sl-slide-inner">
              <div class="bg-img bg-img-3"></div>
              <h2><a href="#">Tempat Parkir</a></h2>
              <blockquote>              
             
              <p>Dilengkapi dengan tempat parkir yang luas.</p>
          
              </blockquote>
            </div>
          </div>
          
          <div class="sl-slide" data-orientation="vertical" data-slice1-rotation="-5" data-slice2-rotation="25" data-slice1-scale="2" data-slice2-scale="1">
            <div class="sl-slide-inner">
              <div class="bg-img bg-img-4"></div>
              <h2><a href="#">Tempat Tidur</a></h2>
              <blockquote>              
              
              <p>Memiliki fasilitas ruang tempat tidur yang nyaman.</p>
              </blockquote>
            </div>
          </div>
          
          <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-5" data-slice2-rotation="10" data-slice1-scale="2" data-slice2-scale="1">
            <div class="sl-slide-inner">
              <div class="bg-img bg-img-5"></div>
              <h2><a href="#">Kamar Mandi</a></h2>
              <blockquote>              
              <p>Dilengkapi dengan kamar mandi yang sangat nyaman dan menyegarkan.</p>

              </blockquote>
            </div>
          </div>
        </div><!-- /sl-slider -->



        <nav id="nav-dots" class="nav-dots">
          <span class="nav-dot-current"></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </nav>

      </div><!-- /slider-wrapper -->
</div>


<!-- banner -->
<div class="container">
  <div class="properties-listing spacer"> <a href="<?php echo base_url();?>home/kamar" class="pull-right viewall">Lihat Semua Kamar</a>
    <h2>Daftar Kamar</h2>
    <div id="owl-example" class="owl-carousel">
      
    	<?php
    	foreach ($kamar->result_array() as $value) { ?>
    		
	      <div class="properties">
	        <div class="image-holder"><img src="<?php echo base_url();?>images/kamar_gambar/<?php echo $value['nama_kamar_gambar'];?>" class="img-responsive" alt="properties"/>
	          <?php
	          if ($value['status_kamar']=="0") { ?>
	          <div class="status sold">Avaliable</div>

	          <?php
	      		}
	          else { ?>

	          <div class="status new">No Avaliable</div>
	          <?php
	      		}
	          ?>
	        </div>
	        <h4><a href="<?php echo base_url();?>home/kamar_detail/<?php echo $value['id_kamar'];?>"><?php echo $value['nomer_kamar'];?></a></h4>
	        <p class="price">Harga: <?php echo rupiah($value['harga_kamar']);?></p>
	        <div class="listing-detail"><?php echo $value['nama_kelas_kamar'];?>   </div>
          
	           <?php if ($value['status_kamar']=="0") { ?>
          <a class="btn btn-primary" href="<?php echo base_url();?>user/kamar_detail/<?php echo $value['id_kamar'];?>">Detail</a>
        <?php } else{ ?>
          <a class="btn btn-danger">Kamar Tidak Tersedia</a>
        <?php } ?>
	      </div>
    	<?php
    	}
    	?>
      
     
      
    </div>
  </div>
  
</div>