<div class="inside-banner">
  <div class="container"><h2>Info Kamar</h2></div>
</div>
<!-- banner -->
<div class="container">
  <div class="properties-listing spacer">
    <?php if(validation_errors()) { ?>
    <div class="alert alert-danger">
      <button type="button" class="close" data-dismiss="alert">×</button>
      <?php echo validation_errors(); ?>
    </div>
    <?php } ?>
    <?php if($this->session->flashdata('berhasil')){ echo "<div class='alert alert-success'><span>Booking Berhasil! Silahkan datang ke kantor untuk melakukan pembayaran</span></div>";}?>
    <div class="row">
      <div class="col-lg-3 col-sm-4 hidden-xs">
        <div class="search-form"><h4><span class="glyphicon glyphicon-search"></span> Search for</h4>
          <?php echo form_open('home/kamar_kelas_kamar');?>
          <div class="row">
            <div class="col-lg-12">
              <select class="form-control" name="kelas_kamar_id">
                <option value="">Pilih Nama Kos</option>
                <?php
                foreach ($kelas_kamar->result_array() as $value) { ?>
                    <option value="<?php echo $value['id_kelas_kamar'];?>"><?php echo $value['nama_kelas_kamar'];?></option>
                <?php
                }
                ?>
              </select>
            </div>
          </div>
          <button class="btn btn-primary">Cari</button>
          <?php echo form_close();?>
        </div>
      </div>    
      <!-- BAGIAN PILIH KAMR -->
      <div class="col-lg-9 col-sm-8 ">
        <?php
        foreach ($kamar->result_array() as $value) {
          $id_kamar         = $value['id_kamar'];
          $nomer_kamar      = $value['nomer_kamar'];
          $harga_kamar      = $value['harga_kamar'];
          $fasilitas_kamar  = $value['fasilitas_kamar'];
          $nama_kelas_kamar = $value['nama_kelas_kamar'];
          $status_kamar     = $value['status_kamar'];
          $alamat           = $value['alamat'];
        }
        ?>
        <h2><?php echo $nomer_kamar;?></h2>
        <div class="row">
          <div class="col-lg-8">
            <div class="property-images">
              <!-- Slider Starts -->
              <div id="myCarousel" class="carousel slide" data-ride="carousel">
               <!-- Indicators -->
                <ol class="carousel-indicators hidden-xs">
                  <?php $no=0; foreach ($kamar_gambar->result_array() as $value) { ?>
                    <?php if ($no==0) { ?>
                       <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <?php } else { ?>
                       <li data-target="#myCarousel" data-slide-to="<?php echo $no;?>" class=""></li>
                  <?php } $no++; } ?>
                </ol>
                <div class="carousel-inner">
                  <?php $no=0; foreach ($kamar_gambar->result_array() as $value) { ?>
                    <?php if ($no==0) { ?>
                      <div class="item active">
                        <img src="<?php echo base_url();?>images/kamar_gambar/<?php echo $value['nama_kamar_gambar'];?>" class="properties" alt="properties" />
                      </div>
                    <?php } else { ?>
                      <div class="item">
                        <img src="<?php echo base_url();?>images/kamar_gambar/<?php echo $value['nama_kamar_gambar'];?>" class="properties" alt="properties" />
                      </div> 
                    <?php } ?>
                  <?php $no++; } ?>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
              </div>
            <!-- #Slider Ends -->
            </div>
            <div class="spacer">
              <h4><span class="glyphicon glyphicon-th-list"></span>Informasi Kamar</h4>
              <p><?php echo $fasilitas_kamar;?></p> 
            </div>
          </div>
          <div class="col-lg-4" style="background: white; padding-top: 20px; padding-bottom: 10px">
            <div class="col-lg-12  col-sm-6">
              <div class="property-info"><p class="price" style="font-size: 24px"><?php echo rupiah($harga_kamar);?> /bulan</p></div>
              <h6><span class="glyphicon glyphicon-home"></span> <?php echo $nama_kelas_kamar;?></h6>
              <h6><span class="glyphicon glyphicon-home"></span> <?php echo $alamat;?></h6>
              <div class="listing-detail">
              </div>
              
                <?php if(!$this->session->userdata("email")) { ?>
                  <a href="<?php echo base_url();?>user/login"><button class="btn btn-success btn-md" style="width: 100%; font-size: 12px"><span class="glyphicon glyphicon-user"></span>&nbsp;Silahkan Login Untuk Memesan</button></a>
                <?php } else{ ?>
                <div class="row">
                <?php echo form_open('user/beli');?>
                  <input type="hidden" name="id_kamar" value="<?php echo $id_kamar;?>">
                  <input type="hidden" name="harga_kamar" value="<?php echo $harga_kamar;?>">
                  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input class="form-control"  type="text" name="tgl_reservasi_masuk" placeholder="Tanggal Masuk">
                  </div>
                  <br>
                  <div class="input-group date form_date col-md-12" data-date="" data-date-format="dd/mm/yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                    <input class="form-control"  type="text" name="tgl_reservasi_keluar" placeholder="Tanggal Keluar">                                        
                  </div>
                </div><br>
                <button class="btn btn-danger btn-md" type="submit" style="width: 100%" name="beli" value="beli"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;Pesan Kamar</button>
                <?php echo form_close();?>
                <?php } ?>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>