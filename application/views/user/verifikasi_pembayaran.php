<div class="inside-banner">
	<div class="container"> 
	    <center><h2>Verifikasi Pembayaran KOS | Nomor Pemesanan <a href=""><strong style="color: blue"><?= $no_pemesanan ?></strong></a></h2></center>
	</div>
</div>
<div class="container" style="background: white">
	<div class="spacer">
		<div class="col-lg-9 col-sm-9 ">
	    <?php if(validation_errors()) { ?>
		    <div class="alert alert-danger">
		      <button type="button" class="close" data-dismiss="alert">×</button>
		      <?php echo validation_errors(); ?>
		    </div>
	    <?php } ?>
		<?php if ($this->session->flashdata('sukses')){ echo "<div class='alert alert-success'><span>Kritik/Saran Anda berhasil dikiirim</span></div>";}?>
		</div>
		<div class="col-lg-3 col-sm-3 ">
		</div>
		<div class="row">
			<center>
				<h2>Total Pembayaran <strong style="color: green"><?= rupiah($biaya) ?></strong></h2>
				<h3>Nomor Rekening <?php echo $bank['nama_bank'] ?>: <strong style="color: orange"><?php echo $bank['no_rekening'] ?></strong> (AN. <?php echo $bank['nama_rekening'] ?>)</h3></center>					
		</div>
		<div class="row contact">
			<div class="col-lg-6 col-sm-6">
				<center><img src="<?php echo base_url();?>images/komponen/pay.png" width="100%"></center>
			</div>
			<div class="col-lg-6 col-sm-6 ">
				<div class="row" style="width: 98%">
					<?php echo form_open_multipart('user/upload_pembayaran');?>
					<input type="hidden" class="form-control" name="no_pemesanan" value="<?= $no_pemesanan ?>">
					<label>Masukan Nomor Rekening</label>
					<input type="text" class="form-control" name="no_rekening" placeholder="Masukan Nomor Rekening">
					<label>Masukan Nama Pemilik Rekening</label>
					<input type="text" class="form-control" name="nama_pemilik" placeholder="Masukan Nama Pemilik Rekening">
					<label>Masukkan Catatan Kepada Pemilik Kos</label>
					<center>
						<textarea rows="3" class="form-control" name="keterangan" placeholder="(Optional)"></textarea>
					</center>
					<div class="form-group">
						<label for="exampleFormControlFile1">Upload Bukti Pembayaran</label>
						<input type="file" name="bukti_foto" class="form-control-file" id="exampleFormControlFile1">
					</div> 
				</div>                 
			</div>
			<center>
				<button type="submit" class="btn btn-success" name="Submit" style="width: 80%">Verifikasi Pembayaran</button>
			</center>
			<?php echo form_close();?> 
		</div>
	</div>
</div>