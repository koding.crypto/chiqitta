<div class="row">
				<div class="col-md-12">
					
					<?php 
									
													if ($this->session->flashdata('hapus')){
									echo "<div class='alert alert-danger'>
												                   <span>Berhasil Menghapus Pemesanan</span>  
												                </div>";
													}
													else if($this->session->flashdata('berhasil')){
														echo "<div class='alert alert-success'>
												                   <span>Berhasil Menambahkan Data</span>  
												                </div>";
													}
													else if($this->session->flashdata('update')){

														echo "<div class='alert alert-success'>
												                   <span>Kategori Galeri Update</span>  
												                </div>";

													}
													else if($this->session->flashdata('ada')){

														echo "<div class='alert alert-danger'>
												                   <span>Kategori Galeri Exist</span>  
												                </div>";

													}
												
							?>
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-money"></i>Daftar Pesanan
							</div>


							
						</div>

						<div class="portlet-body">
							<table class="table table-striped table-hover table-bordered" id="sample_editable_1">
							<thead>
							<tr>
								<th>No</th>
								<th>Nomor Pemesanan</th>
								<th>Detail Kamar</th>
								<th>Tanggal Sewa</th>
								<th>Jumlah Hari</th>
								<th>Biaya</th>
								<th>Status</th>
								<th>Aksi</th>		
							</tr>
							</thead>
							<tbody>
									<?php
										$no=1;
										
											foreach ($pesanan_pengguna->result_array() as $tampil) { ?>
										<tr >
											<td><?php echo $no;?></td>
											<td><?php echo $tampil['no_pemesanan'];?></td>
											<td>
												<strong>Nomor Kamar</strong> :<br>
												<?php echo $tampil['nomer_kamar'];?><br>
												<strong>Harga Perbulan</strong> :<br>
												<?php echo $tampil['harga_kamar'];?><br>
												<strong>Alamat</strong> :<br>
												<?php echo $tampil['alamat'];?><br><br>
												<strong>Detail</strong>:<br>
												<?php echo $tampil['fasilitas_kamar'];?><br>
												
											</td>
											<td><?php echo $tampil['tgl_reservasi_masuk'];?> - <?php echo $tampil['tgl_reservasi_keluar'];?></td>
											<td><?php echo $tampil['hari'];?></td>
											<td><?php echo $tampil['biaya'];?></td>
											<td>
												<?php if($tampil['status'] == 0){?>
													<span>Belum Melakukan Transfer</span>
												<?php } elseif($tampil['status'] == 1){?>
													<span>Sudah Melakukan Transfer</span>
												<?php } elseif($tampil['status'] == 2){?>
													<span>Melakukan Pembatalan</span>
											<?php } elseif($tampil['status'] == 3){?>
													<span style="color: green">Transaksi Berhasil</span>
												<?php } ?>

											</td>
											<td>
												<a href="<?php echo base_url();?>user/hapusdata/<?php echo $tampil['no_pemesanan'];?>"><button class="btn btn-sm btn-danger"><i class="fa fa-times"></i>&nbsp;Hapus</button></a>&nbsp;
												<a href="<?php echo base_url();?>user/pembatalan/<?php echo $tampil['no_pemesanan'];?>"><button class="btn btn-sm btn-warning"><i class="fa fa-times"></i>&nbsp;Pembatalan</button></a>&nbsp;
												<a href="<?php echo base_url();?>user/verifikasi_pembayaran/<?php echo $tampil['no_pemesanan'];?>"><button class="btn btn-sm btn-info"><i class="fa fa-upload"></i>&nbsp;Upload Bukti Transaksi</button></a>
											</td>
	<!-- 										<td><a  href="<?php echo base_url();?>sistem/kategori_galeri_edit/<?php echo $tampil['id_kategori_galeri'];?>"><i class="fa fa-edit"></i></a> &nbsp;
											<a  href="<?php echo base_url();?>sistem/kategori_galeri_delete/<?php echo $tampil['id_kategori_galeri'];?>" onclick="return confirm('Yakin Ingin Menghapus <?php echo $tampil['nama_kategori_galeri'];?>?')"> <i class="fa fa-times"></i></a></td>
											 -->
											
										</tr>
										<?php
										$no++;
										}
										?>
										
										
										
							</tbody>
							</table>
						</div>
					</div>
					
				</div>
			</div>