<div class="container" style="background: white">
	<div class="spacer">
		<div class="col-lg-9 col-sm-9 ">
	    <?php if(validation_errors()) { ?>
		    <div class="alert alert-danger">
		      <button type="button" class="close" data-dismiss="alert">×</button>
		      <?php echo validation_errors(); ?>
		    </div>
	    <?php } ?>
		<?php if ($this->session->flashdata('sukses')){ echo "<div class='alert alert-success'><span>Kritik/Saran Anda berhasil dikiirim</span></div>";}?>
		</div>
		<div class="row contact" style="vertical-align: center">
			<div class="col-lg-6 col-sm-6">
				<center>
					<img src="<?php echo base_url();?>images/komponen/portal.png" width="90%">
				</center>
			</div>
			<div class="col-lg-6 col-sm-6 ">
				<div class="row" style="width: 90%">
					<center>
						<h3>Halaman Login Pengguna</h3>
						<?php echo form_open('user/login');?>
							<input type="text" class="form-control" name="email" placeholder="Masukan Alamat Email">
							<input type="password" class="form-control" name="password" placeholder="Masukan Password">
							<button type="submit" class="btn btn-success" name="submit" value="submit" style="width: 70%">Login</button><br><br>
							<h4>Belum Punya Akun? <a href="<?php echo base_url();?>user/pendaftaran">Daftar Sekarang</a></h4>
						<?php echo form_close();?>
					</center>
					  
				</div>                 
			</div>
		</div>
	</div>
</div>