<?php

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(['sistem_model','home_model','user_model']);
	}

	public function pendaftaran () {
		if($this->input->post('prosesPendaftaran') != ""){
			$this->user_model->registrasi();
			redirect('user/login');
		}
		$data['tentang_kami'] = $this->sistem_model->TentangKami();
		$this->template_home->load('template_home','user/pendaftaran',$data);	
	}

	public function login () {
		if($this->input->post('submit') != ""){
			$email = $this->input->post('email');
            $password = md5($this->input->post('password'));
            $user = $this->user_model->login($email);
            if($password == $user->password){
                $data = [
                    'user_id' => $user->id,
                    'email' => $user->email,
                    'logged_in' => true
                ];
                $this->session->set_userdata($data);
                $this->session->set_flashdata('success', 'You are now logged in');
                redirect('home/kamar');
            } else {
                $this->session->set_flashdata('danger', 'Login is invalid');
                redirect('user/login');
            }
		}

		$data['tentang_kami'] = $this->sistem_model->TentangKami();
		$this->template_home->load('template_home','user/login',$data);
	}

	public function kamar_detail () {
		$id = $this->uri->segment(3);
		if(!$id){
			redirect("home/kamar");
		}
		$data['tentang_kami'] 	= $this->home_model->TentangKami();
		$data['kamar'] 			= $this->home_model->KamarDetail($id);
		$data['kamar_gambar'] 	= $this->home_model->KamarGambarId($id);
		$data['kelas_kamar'] 	= $this->home_model->KelasKamar();
		$this->template_home->load('template_home','user/kamar_detail',$data);
	}
	public function verifikasi_pembayaran() {
		$id = $this->uri->segment(3);
		if(!$id){
			redirect("user/pesanan");
		}
		$bank = $this->sistem_model->TentangKami();
		$bank=  $bank->result_array();
		$data['bank'] = $bank[0];

		$data['tentang_kami'] = $this->sistem_model->TentangKami();
		$data['no_pemesanan'] = $id;
		$detail_pemesanan = $this->user_model->ReadUploadPesanan($id);
		$data['biaya'] = $detail_pemesanan->biaya;

		$this->template_home->load('template_home','user/verifikasi_pembayaran',$data);
	}

	public function pesanan($id = null) {
		if($this->session->userdata("user_id")!=="" ) {

			$data['pesanan_pengguna']	= $this->user_model->ReadPesananpengguna($this->session->userdata("user_id"));
			$this->template_system->load('template_user','user/pesanan',$data);
		}
		else{
			redirect('sistem');

		}
	}
	public function logout(){
        $this->session->unset_userdata(['user_id', 'email', 'logged_in']);
        $this->session->sess_destroy();
        $this->session->set_flashdata('success', 'You are now logged out');
        redirect('user/login');
    }


    /*PEMESANAN KAMAR*/
    public function beli() {
    	if($this->session->userdata("user_id")!=="" ) {
    		if($this->input->post('beli') != ""){
    			$in['no_pemesanan'] 		= $this->acak(12);
    			$in['id_pengguna'] 			= $this->session->userdata("user_id");
				$in['id_kamar'] 			= $this->input->post('id_kamar');
				$harga_kamar 				= $this->input->post('harga_kamar');
				$tgl_reservasi_masuk 		= $this->input->post('tgl_reservasi_masuk');
				$tgl_reservasi_keluar		= $this->input->post('tgl_reservasi_keluar');
				$masuk 						= date_create($this->tanggal($tgl_reservasi_masuk));
				$keluar 					= date_create($this->tanggal($tgl_reservasi_keluar));
				$hari 						= date_diff($masuk,$keluar);
				$in['tgl_reservasi_masuk'] 	= $tgl_reservasi_masuk;
				$in['tgl_reservasi_keluar'] = $tgl_reservasi_keluar;
				$in['hari'] 				= $hari->days;
				$in['biaya'] 				= ceil($hari->days/30)*$harga_kamar;	
				$in['status'] 				= 0;								
				$this->db->insert("pemesanan",$in);

				$data['no_pemesanan'] = $in['no_pemesanan'];
				$data['no_rekening'] = "";
				$data['nama_pemilik'] = "";
				$data['keterangan'] = "";
				$data['foto'] = "";

				$this->db->insert("upload_pembayaran",$data);

				$this->session->set_flashdata('berhasil memesan kamar','OK');
				redirect("user/pesanan");
    		}
    	}
    	else{
    		redirect("home/kamar");
    	}

	}

	public function tanggal($tanggal){
		$arr = explode("/", $tanggal);
		$format[0] = $arr[2];
		$format[1] = $arr[1];
		$format[2] = $arr[0];
		return implode("-", $format);
	}

	function acak($length = 12) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function upload_pembayaran(){
		$config = array(
			'file_name' => $this->input->post('no_pemesanan'),
			'upload_path' => "./bukti_pembayaran/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => TRUE,
			'max_size' => 0,
			'max_width' => 0,
			'max_height' => 0,
			'encrypt_name' => TRUE
		);
		$this->load->library('upload', $config);
		if($this->upload->do_upload('bukti_foto')){
			$data = $this->upload->data();
			$foto =  $data['file_name'];
			$in['no_rekening'] 		= $this->input->post('no_rekening');
			$in['nama_pemilik'] 	= $this->input->post('nama_pemilik');
			$in['keterangan'] 		= $this->input->post('keterangan');
			$in['foto'] 			= $foto;
			$where = array(
				'no_pemesanan' => $this->input->post('no_pemesanan')
			);
			$this->user_model->UpdateBuktiFoto($where,$in);
			$this->db->where($where);
			$update_status['status'] = 1;
	        $this->db->update('pemesanan',$update_status);
			redirect("user/pesanan");
		}
	}

	public function hapusdata() {
		if($this->session->userdata("user_id")!=="" ) {
			$id = $this->uri->segment(3);
			$data = $this->db->query("select * from upload_pembayaran where no_pemesanan='$id' limit 1");
			$data = $data->result();
			$data_pembayaran = $data[0];
			if(isset($data_pembayaran->foto)){
				$foto = './bukti_pembayaran/'.$data_pembayaran->foto;
				unlink($foto);
				$this->user_model->DeleteBuktiFoto($id);
				$this->user_model->DeletePesanan($id);
				// print($foto);
				$this->session->set_flashdata('hapus','ok');
				redirect("user/pesanan");
			}
		}
		else{
			redirect('sistem');

		}

	}

	public function pembatalan() {
		if($this->session->userdata("user_id")!=="" ) {
			$id = $this->uri->segment(3);
			$where = array(
				'no_pemesanan' => $id
			);
			$update_status['status'] = 2;
			$this->db->where($where);
	        $this->db->update('pemesanan',$update_status);
	        $this->session->set_flashdata('hapus','ok');
			redirect("user/pesanan");
		}
		else{
			redirect('sistem');

		}

	}
}



