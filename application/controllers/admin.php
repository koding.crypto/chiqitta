<?php

class Admin extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(['sistem_model','home_model','user_model','admin_model']);
	}
	public function pesanan($id = null) {
		if($this->session->userdata("user_id")!=="" ) {
			$kode = $this->uri->segment(3);
			if($kode == "booking"){
				$data['pesanan_pengguna']	= $this->admin_model->ReadPesananpenggunaPer('0');
			}
			elseif($kode == "belum"){
				$data['pesanan_pengguna']	= $this->admin_model->ReadPesananpenggunaPer('1');
			}
			elseif($kode == "berhasil"){
				$data['pesanan_pengguna']	= $this->admin_model->ReadPesananpenggunaPer('3');
			}
			elseif($kode == "batal"){
				$data['pesanan_pengguna']	= $this->admin_model->ReadPesananpenggunaPer('2');
				//ReadPesananpenggunaPer($kode)
			}
			else{
				$data['pesanan_pengguna']	= $this->admin_model->ReadPesananpengguna();
			}
			$this->template_system->load('template_system','admin/pesanan',$data);
		}
		else{
			redirect('sistem');

		}
	}

	function acak($length = 12) {
	    $characters = '0123456789';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function upload_pembayaran(){
		$config = array(
			'file_name' => $this->input->post('no_pemesanan'),
			'upload_path' => "./bukti_pembayaran/",
			'allowed_types' => "gif|jpg|png|jpeg|pdf",
			'overwrite' => TRUE,
			'max_size' => 0,
			'max_width' => 0,
			'max_height' => 0,
			'encrypt_name' => TRUE
		);
		$this->load->library('upload', $config);
		if($this->upload->do_upload('bukti_foto')){
			$data = $this->upload->data();
			$foto =  $data['file_name'];
			$in['no_rekening'] 		= $this->input->post('no_rekening');
			$in['nama_pemilik'] 	= $this->input->post('nama_pemilik');
			$in['keterangan'] 		= $this->input->post('keterangan');
			$in['foto'] 			= $foto;
			$where = array(
				'no_pemesanan' => $this->input->post('no_pemesanan')
			);
			$this->user_model->UpdateBuktiFoto($where,$in);
			$this->db->where($where);
			$update_status['status'] = 1;
	        $this->db->update('pemesanan',$update_status);
			redirect("user/pesanan");
		}
	}

	public function hapusdata() {
		if($this->session->userdata("user_id")!=="" ) {
			$id = $this->uri->segment(3);
			$data = $this->db->query("select * from upload_pembayaran where no_pemesanan='$id' limit 1");
			$data = $data->result();
			$data_pembayaran = $data[0];
			if(isset($data_pembayaran->foto)){
				$foto = './bukti_pembayaran/'.$data_pembayaran->foto;
				unlink($foto);
				$this->user_model->DeleteBuktiFoto($id);
				$this->user_model->DeletePesanan($id);
				// print($foto);
				$this->session->set_flashdata('hapus','ok');
				redirect("admin/pesanan");
			}
		}
		else{
			redirect('sistem');

		}

	}

	public function pembatalan() {
		if($this->session->userdata("user_id")!=="" ) {
			$id = $this->uri->segment(3);
			$where = array(
				'no_pemesanan' => $id
			);
			$update_status['status'] = 2;
			$this->db->where($where);
	        $this->db->update('pemesanan',$update_status);

	        $data = $this->db->query("select * from pemesanan where no_pemesanan='$id' limit 1");
			$data = $data->result();
			$data_pemesanan = $data[0];
			if(isset($data_pemesanan->id_kamar)){
				$where = array(
				'id_kamar' => $data_pemesanan->id_kamar
				);
				$update_kamar['status_kamar'] = 0;
				$this->db->where($where);
		        $this->db->update('kamar',$update_kamar);
			}

	        $this->session->set_flashdata('hapus','ok');
			redirect("admin/pesanan");
		}
		else{
			redirect('sistem');

		}

	}

	public function terima() {
		if($this->session->userdata("user_id")!=="" ) {
			$id = $this->uri->segment(3);
			$where = array(
				'no_pemesanan' => $id
			);
			$update_status['status'] = 3;
			$this->db->where($where);
	        $this->db->update('pemesanan',$update_status);

	        $data = $this->db->query("select * from pemesanan where no_pemesanan='$id' limit 1");
			$data = $data->result();
			$data_pemesanan = $data[0];
			if(isset($data_pemesanan->id_kamar)){
				$where = array(
				'id_kamar' => $data_pemesanan->id_kamar
				);
				$update_kamar['status_kamar'] = 1;
				$this->db->where($where);
		        $this->db->update('kamar',$update_kamar);
			}
	        $this->session->set_flashdata('hapus','ok');
			redirect("admin/pesanan");
		}
		else{
			redirect('sistem');

		}

	}


}



