<?php 
class User_model extends CI_Model{
    public function __construct(){
        $this->load->database();
    }

    public function registrasi(){
        $in['nama_lengkap']         = $this->input->post('nama_lengkap');
        $in['nama_ortu']        = $this->input->post('nama_ortu');
        $in['nomor_telepon']        = $this->input->post('nomor_telepon');
        $in['nomor_ortu']       = $this->input->post('nomor_ortu');
        $in['alamat']       = $this->input->post('alamat');
        $in['email']        = $this->input->post('email');
        $in['password']         = md5($this->input->post('password'));
        return $this->db->insert("pengguna",$in);
    }

    public function login($email){
        return $this->db->get_where('pengguna', [ 'email' => $email])->row();
    }


    function KamarDetail($id) {
        return $this->db->query("select a.*,b.* from kamar a
        join kelas_kamar b on a.kelas_kamar_id=b.id_kelas_kamar
        where a.id_kamar='$id' ");
    }


    //PESANAN
     // function ReadPesananpengguna($id_pengguna){
     //    return $this->db->query("select * from pemesanan where id_pengguna='$id_pengguna' order by id asc");
     // }

     function ReadPesananpengguna($id_pengguna){
        return $this->db->query("select a.*,b.*,c.* from pemesanan 
            a join kamar b on a.id_kamar=b.id_kamar
            join upload_pembayaran c on a.no_pemesanan=c.no_pemesanan
            where id_pengguna='$id_pengguna' order by a.id asc
        ");
     }

     function DeletePesanan($no_pemesanan) {
        return $this->db->query("delete from pemesanan where no_pemesanan='$no_pemesanan' ");
     }
     function DeleteBuktiFoto($no_pemesanan) {
        return $this->db->query("delete from upload_pembayaran where no_pemesanan='$no_pemesanan' ");
     }

     function UpdateBuktiFoto($where,$data){
        $this->db->where($where);
        $this->db->update('upload_pembayaran',$data);
    }   







     //Upload
     function ReadUploadPesanan($no_pemesanan){
        return $this->db->get_where('pemesanan', [ 'no_pemesanan' => $no_pemesanan])->row();
     }


     function DeleteUploadPesanan($id) {
        return $this->db->query("delete from kategori_galeri where id_kategori_galeri='$id' ");
     }

}